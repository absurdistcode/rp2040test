const std = @import("std");

const rp2040 = @import("rp2040");

// Raspberry pi example
// Use "$ zig build openocdFlash" to build and flash using openocd
// Use "$ zig build uf2flash" to build and flash using uf2

pub fn build(b: *std.Build) void {
    const optimize = b.standardOptimizeOption(.{});

    const exe = rp2040.addPiPicoExecutable(b, .{
        .name = "rp2040test",
        .source_file = .{ .path = "src/main.zig" },
        .optimize = optimize,
    });
    exe.installArtifact(b);

    addOpenOcdFlash(b, exe.inner, "openocdFlash");
    addUf2Flash(b, exe.inner, "uf2flash");
}

fn addOpenOcdFlash(b: *std.Build, exe: *std.Build.CompileStep, name: []const u8) void {
    const openocd_step = b.addSystemCommand(&[_][]const u8{
        "openocd",
        "-f",
        "interface/cmsis-dap.cfg",
        "-c",
        "adapter speed 5000",
        "-f",
        "target/rp2040.cfg",
        "-c",
    });
    openocd_step.addPrefixedFileArg("program ", exe.getEmittedBin());
    //openocd_step.addArtifactArg(exe);
    openocd_step.addArgs(&[_][]const u8{ "-c", "reset run", "-c", "exit" });

    const openocd = b.step(name, "flash using openocd");
    openocd.dependOn(&openocd_step.step);
}

fn addUf2Flash(b: *std.Build, exe: *std.Build.CompileStep, name: []const u8) void {
    const rp2040_dep = b.dependency("rp2040", .{});
    const rp2040_flasher = rp2040_dep.artifact("rp2040-flash");

    const uf2_file = elf2uf2(b, exe);

    const run_flash = b.addRunArtifact(rp2040_flasher);
    run_flash.addArg("--wait");
    run_flash.addFileSourceArg(uf2_file);

    const flash_step = b.step(name, "flashes the system");
    flash_step.dependOn(&run_flash.step);
}

fn elf2uf2(b: *std.Build, exe: *std.Build.CompileStep) std.Build.LazyPath {
    const uf2_dep = b.dependency("uf2", .{});

    const elf2uf2_run = b.addRunArtifact(uf2_dep.artifact("elf2uf2"));
    elf2uf2_run.addArgs(&.{ "--family-id", "RP2040" });

    elf2uf2_run.addArg("--elf-path");
    elf2uf2_run.addArtifactArg(exe);

    elf2uf2_run.addArg("--output-path");
    const uf2_file = elf2uf2_run.addPrefixedOutputFileArg("", "test.uf2");

    const install_uf2 = b.addInstallFile(uf2_file, "test.uf2");
    install_uf2.dir = .{ .custom = "firmware" };

    b.getInstallStep().dependOn(&install_uf2.step);

    return uf2_file;
}
